---
layout: handbook-page-toc
title: "Trade Compliance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## General Information
1. What is Trade Compliance (Export/Import Control)?
    *  It's US law - find details here: [Trade Compliance: Import & Export Control](/handbook/people-group/code-of-conduct/#trade-compliance-exportimport-control)
    *  Related terms include "embargoed countries", "denied parties", "Export Administration Regulations", 
1. How do we comply? 
   * We use a compliance tool called Visual Compliance which flags for review accounts that are created in SFDC whether by a person or by a system that appear to be in violation every 15 minutes.
   * We don't include the billing country in our checkout portal from embargoed countries.
   * We train Sales how to comply with the law.
1. How often does Legal review the flagged accounts in SFDC?
   * Three times per day.  

## Salesforce
### Effect on accounts
* [Detailed Documentation](/handbook/business-ops/order-processing/#trade-compliance-export--import-and-visual-compliance-tool-in-salesforce)
* High level: Salesforce is integrated with a software application which scans SFDC every fifteen minutes looking for Accounts that match a deny list and mark it for review which a legal team member reviews. They will either clear the account or lock it.
### Effect on opportunities
* [Detailed Documentation](/handbook/business-ops/resources/#locking-opportunities-as-a-result-of-their-at-risk-potential)
* High level: Opportunities are screen on creation by Visual Compliance.
* Effect on Leads/Contacts: checkbox `ECCN Quarantine` marked by Marketo based on IP address

## Customer
### Effect on customer facing web pages
* Embargoed countries will not appear in the list of countries in any embedded web forms on the website for buying or trialing the product with the exception of Ukraine which has an extra step to check a box that the person does not live in the Crimenean Region and they must add their city. This would be checked against Visual Compliance if the person became a Bill-to or Sold-to contact in an order.
* Embargoed countries are not in the list of countries in the checkout process for the portal `customers.gitlab.com` or in the trial forms.
* Embargoed countries may be in the list of in-product forms.
* We do not block at the domain level for `about.gitlab.com` or for `gitlab.com`.
* IP Addresses from the embargoed country list are [blocked from accessing the portal](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/600) and will see message
> In light of the recent discussions around blocking of US-sanctioned countries, we wanted to remind our customers that GitLab is unable to conduct business with individuals or companies located in US embargoed countries, including Crimea, Cuba, Iran, North Korea, Sudan and Syria. This is required under US Export Regulations, as well as, our status as a federal contractor. For more information, please visit our [Trade Compliance page](/handbook/people-operations/code-of-conduct/#trade-compliance-exportimport-control)
> 
> If you feel you have reached this page in error, please contact [GitLab Support](https://support.gitlab.com/hc/en-us)

## Future State Considerations
* Currently, contacts in embargoed countries cannot access the customer portal. Block generation of license keys for any contact with an embargoed country. This could be a potential future state scenario if we link all contacts to accounts. If the contact were to contact sales or marketing and need to fix an issue with their account and they've been added as a billing contact, they should not be able to receive a license key.

## Links
*  [Legal Team's related KPI](/handbook/legal/#all-suspicious-transactions-are-cleared-actioned-or-escalated---1-business-day)
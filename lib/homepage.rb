module Gitlab
  module Homepage
    autoload :Team, 'lib/team'
    autoload :Event, 'lib/event'
    autoload :DevopsTool, 'lib/devops_tool'
    autoload :Category, 'lib/category'
    autoload :Stage, 'lib/stage'
    autoload :Feature, 'lib/feature'
    autoload :CareerMatrix, 'lib/career_matrix'

    class Team
      autoload :Member, 'lib/team/member'
      autoload :Project, 'lib/team/project'
      autoload :Assignment, 'lib/team/assignment'
    end

    class Jobs
      autoload :JobsListing, 'lib/jobs/jobs_listing'
      autoload :HiringStatus, 'lib/jobs/hiring_status'
    end
  end
end
